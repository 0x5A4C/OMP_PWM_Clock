#include <Arduino.h>
#include <Time.h>
#include <SPI.h> 

#include "mod/displayPwm.h"
#include "mod/displaySer.h"
#include "mod/setTimeNtp.h"
#include "mod/setTimeRtc.h"
#include "mod/setUpWiFi.h"
// #include "mod/heartBeat.h"
// #include "mod/webServer.h"
// #include "mod/getTempPress.h"
// #include "mod/blynkFeed.h"
#include "mod/i2c.h"
#include "mod/log.h"

void setup()
{
	init_log();
	init_i2c();
	init_setTimeRtc();
	init_setUpWiFi();
	// // init_heartBeat();
	init_displayPwm();
	init_dispSer();
	init_setTimeNtp();
	// init_webServer();
	// // init_getTempPress();
	// // init_blynkFeed();
}

void loop()
{
	// process_i2c();
	// process_setTimeRtc();
	// // process_heartBeat();
	// process_dispSer();
	// process_setUpWiFi();
	// process_setTimeNtp();
	// process_webServer();
	// // process_getTempPress();
	// // process_blynkFeed();
}

