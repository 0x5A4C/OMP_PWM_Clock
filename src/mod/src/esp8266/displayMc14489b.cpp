#include <mod/displayMc14489b.h>
#include <mod/log.h>
#include <mod/mc14489b.h>
#include <mod/getTempPress.h>

#include <Arduino.h>
#include <Ticker.h>
#include <Time.h>

#define CLOCK D5
#define ENABLE D6
#define DATA D7

Ticker timer_displayLed;
Mc14489b mc14489B(DATA, CLOCK, ENABLE);

void test_displayMc14489b()
{
	mc14489B.control(B00000001);
	mc14489B.digit0(0);
	mc14489B.digit1(1);
	mc14489B.digit2(2);
	mc14489B.digit3(3);
	mc14489B.digit4(4);
	mc14489B.update();
}

void display_time()
{
	mc14489B.control(B01001001);

	mc14489B.setDot(0);
	mc14489B.digit0(hour() / 10);
	mc14489B.digit1(hour() % 10);
	mc14489B.digit2((second() & 0x01 ? 0x0d : 0x00));
	mc14489B.digit3(minute() / 10);
	mc14489B.digit4(minute() % 10);
	mc14489B.update();
}

void display_temp()
{
	mc14489B.control(B10010001);

	mc14489B.setDot(2);
	mc14489B.digit0((int)temp_getTempPress() / 10);
	mc14489B.digit1((int)temp_getTempPress() % 10);
	mc14489B.digit2((int)(temp_getTempPress() * 10) % 10);
	mc14489B.digit3(0x0f);
	mc14489B.digit4(0x0c);
	mc14489B.update();
}

void update_displayLed()
{
	switch (second())
	{
	case 15:
	case 16:
	case 17:
	case 30:
	case 31:
	case 32:
	case 45:
	case 46:
	case 47:
		display_temp();
		break;
	default:
		display_time();
		break;
	}
}

void init_displayMc14489b()
{
	Logger.debug.println("displayMc14489b: init");

	// mc14489B.test();

	timer_displayLed.attach(1, update_displayLed);
}

void process_displayMc14489b()
{
	// Logger.debug.println("displayMc14489b: process");
}

void close_displayMc14489b()
{
	Logger.debug.println("displayMc14489b: close");
}
