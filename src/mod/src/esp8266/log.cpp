#include <mod/log.h>

#include <Arduino.h>

void init_log()
{
  Serial.begin(115200);
  while(!Serial)
  {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Logger.addHandler(Logger.DEBUG, Serial);

  Logger.debug.println("log: init");
  // Logger.info.println("");
  // Logger.warning.println("");
  // Logger.error.println("");
}

void process_log()
{
  Logger.debug.println("process");
}

void close_log()
{
  Logger.debug.println("close");

  Logger.removeHandler(Serial);
}
