#include <mod/displayPwm.h>
#include <mod/log.h>

#include <ESP8266WiFi.h>
#include <Ticker.h>
#include <Time.h>

#define PIN_PWM_SECONDS D5
#define PIN_PWM_MINUTES D7
#define PIN_PWM_HOURS   D6

#define PWMRANGE 375

Ticker timer_displayPwm;

uint16_t counter = 0;

void update_displayPwm()
{
	if (year() > 2014)
	{
		// analogWrite(PIN_PWM_SECONDS, PWMRANGE/60*second());
		// analogWrite(PIN_PWM_MINUTES, PWMRANGE/60*minute());
		// analogWrite(PIN_PWM_HOURS, PWMRANGE/24*hour());

		// map(value, fromLow, fromHigh, toLow, toHigh)
		analogWrite(PIN_PWM_SECONDS, map(second(),  0, 60, 0, PWMRANGE-150));
		analogWrite(PIN_PWM_MINUTES, map(minute(),  0, 60, 0, PWMRANGE-150));
		analogWrite(PIN_PWM_HOURS, map(hour(),  0, 24, 0, PWMRANGE-150));

	}
	else
	{
		analogWrite(PIN_PWM_SECONDS, counter);
		analogWrite(PIN_PWM_MINUTES, counter);
		analogWrite(PIN_PWM_HOURS, counter);

		counter = counter > PWMRANGE ? 0 : counter + (PWMRANGE / 10);
	}
}

void init_displayPwm()
{
	Logger.debug.println("display pwm: init");

	analogWrite(PIN_PWM_SECONDS, 500);
	analogWrite(PIN_PWM_MINUTES, 500);
	analogWrite(PIN_PWM_HOURS,   500);

	timer_displayPwm.attach(1, update_displayPwm);
}

void process_displayPwm()
{
	Logger.debug.println("process");
}

void close_displayPwm()
{
	Logger.debug.println("close");
}
