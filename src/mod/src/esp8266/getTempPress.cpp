
#include "Arduino.h"

#include <mod/getTempPress.h>
#include <mod/log.h>

#include <Wire.h>
#include <BMP085.h>
#include <BMP280.h>

#include <Ticker.h>

Ticker timer_updateTempPress;

BMP085 bm085_sensor;
BMP280 bm280_sensor;

static float temperature = 0;
static float pressure = 0;
static int32_t altitude = 0;

float temp_getTempPress()
{
	return temperature;
}

float press_getTempPress()
{
	return pressure;
}

int32_t alt_getTempPress()
{
	return altitude;
}

String getTempAsString_getTempPress()
{
	auto str = String(((int)temp_getTempPress() / 10), DEC) +
			   String(((int)temp_getTempPress() % 10)) + "." + String(((int)(temp_getTempPress() * 10) % 10));
	return str;
}

String getPressAsString_getTempPress()
{
	auto str = String(((int)press_getTempPress() / 1000), DEC) +
			   String(((int)press_getTempPress() / 100), DEC) +
			   String(((int)press_getTempPress() / 10), DEC) +
			   String(((int)press_getTempPress() % 10)) + "." + String(((int)(press_getTempPress() * 10) % 10));
	return str;
}

String getAltAsString_getTempPress()
{
	auto str = String(((int)alt_getTempPress() / 1000), DEC) +
			   String(((int)alt_getTempPress() / 100), DEC) +
			   String(((int)alt_getTempPress() / 10), DEC) +
			   String(((int)alt_getTempPress() % 10)) + "." + String(((int)(alt_getTempPress() * 10) % 10));
	return str;
}

void updateTempPress_bmp085()
{
	bm085_sensor.setControl(BMP085_MODE_TEMPERATURE);
	temperature = bm085_sensor.getTemperatureC();

	bm085_sensor.setControl(BMP085_MODE_PRESSURE_3);
	pressure = bm085_sensor.getPressure();
	altitude = bm085_sensor.getAltitude(pressure);
}

void updateTempPress_bmp280()
{

	double T, P, P0 = 1020;
	char result = bm280_sensor.startMeasurment();

	if (result != 0)
	{
		delay(result);
		result = bm280_sensor.getTemperatureAndPressure(T, P);
		temperature = (float)T;
		pressure = (float)P;

		if (result != 0)
		{
			double A = bm280_sensor.altitude(P, P0);
			altitude = (float)A;
		}
		else
		{
			// Serial.println("Error.");
		}
	}
	else
	{
		// Serial.println("Error.");
	}
}

std::function<void()> updateTempPress;

void init_getTempPress()
{
	Logger.debug.println("getTempPress: init");

	if (bm085_sensor.testConnection())
	{
		updateTempPress = updateTempPress_bmp085;
	}
	else if (bm280_sensor.begin())
	{
		bm280_sensor.setOversampling(4);
		updateTempPress = updateTempPress_bmp280;
	}

	if (updateTempPress)
	{
		timer_updateTempPress.attach(1, updateTempPress);
	}
}

void process_getTempPress()
{
	Logger.debug.println("getTempPress: process");
}

void close_getTempPress()
{
	Logger.debug.println("getTempPress: close");
}
