#include <mod/cli.h>

void Splitter::split(const std::string &str, const std::string &delim, std::vector<std::string> &parts)
{
    std::size_t start, end = 0;
    while (end < str.size())
    {
        start = end;
        while (start < str.size() && (delim.find(str[start]) != std::string::npos))
        {
            start++; // skip initial whitespace
        }
        end = start;
        while (end < str.size() && (delim.find(str[end]) == std::string::npos))
        {
            end++; // skip to end of word
        }
        if (end - start != 0)
        { // just ignore zero-length strings.
            parts.push_back(std::string(str, start, end - start));
        }
    }
}

class Cmd : public Splitter
{
private:
protected:
    String out = "";

public:
    virtual String cmd(std::vector<std::string> &parts) = 0;
    virtual String help() = 0;
};

class HelpCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        return "Some help";
    };
    virtual String help()
    {
        return "Some help on help";
    };
};

extern String getIpAsString_setUpWiFi();
class GetIpCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        out = String("IP: ") + getIpAsString_setUpWiFi();
        return out;
    };
    virtual String help()
    {
        return String("Print current IP.");
    };
};

extern String getAsString_RtcTime();
class GetRtcTimeCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        return (getAsString_RtcTime());
    };
    virtual String help()
    {
        return String("RTC time.");
    };
};

extern String getTempAsString_getTempPress();
class GetTempCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        return (getTempAsString_getTempPress());
    };
    virtual String help()
    {
        return String("Current temperature.");
    };
};

extern String getPressAsString_getTempPress();
class GetPressCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        return (getPressAsString_getTempPress());
    };
    virtual String help()
    {
        return String("Curenet press.");
    };
};

extern String getAltAsString_getTempPress();
class GetAltCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        return (getAltAsString_getTempPress());
    };
    virtual String help()
    {
        return String("Current alt.");
    };
};

extern String getLocalTimeAsString();
class GetLocalTimeCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        return getLocalTimeAsString();
    };
    virtual String help()
    {
        return String("Local time.");
    };
};

extern void setLocalTimeAsString(String h, String m, String s);
class SetLocalTimeCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        if (parts.size() >= 2)
        {
            out = String("Set local time from: ") + "\n" + String(parts[1].data()) + "\n";
            std::vector<std::string> timeParts;
            split(parts[1], ":", timeParts);
            setLocalTimeAsString(String(timeParts[0].data()), String(timeParts[1].data()), String(timeParts[2].data()));
        }
        return out;
    };
    virtual String help()
    {
        return "Set local time.";
    };
};

extern String getSsid();
class GetSsidCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        return getSsid();
    };
    virtual String help()
    {
        return "Get sid.";
    };
};

extern void setSsid(String newSsidStr);
class SetSsidCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        if (parts.size() >= 2)
        {
            out = String("Set ssid to: ") + "\n" + String(parts[1].data()) + "\n";
            setSsid(String(parts[1].data()));
        }
        return out;
    };
    virtual String help()
    {
        return "Set ssid.";
    };
};

extern String getPass();
class GetPassCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        return getPass();
    };
    virtual String help()
    {
        return "Get pass.";
    };
};

extern void setPass(String newPassStr);
class SetPassCmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        if (parts.size() >= 2)
        {
            out = String("Set pass to: ") + "\n" + String(parts[1].data()) + "\n";
            setPass(String(parts[1].data()));
        }
        return out;
    };
    virtual String help()
    {
        return "Set ssid.";
    };
};

extern void restartWiFI_setUpWiFi();
class RestartWiFICmd : public Cmd
{
public:
    virtual String cmd(std::vector<std::string> &parts)
    {
        restartWiFI_setUpWiFi();
        return out;
    };
    virtual String help()
    {
        return "Restart WiFi.";
    };
};

std::map<std::string, Cmd *> commands = {
    {"help", new HelpCmd()},
    {"getip", new GetIpCmd()},
    {"getrtctime", new GetRtcTimeCmd()},
    {"gettemp", new GetTempCmd()},
    {"getpress", new GetPressCmd()},
    {"getalt", new GetAltCmd()},
    {"getlocaltime", new GetLocalTimeCmd()},
    {"setlocaltime", new SetLocalTimeCmd()},
    {"getssid", new GetSsidCmd()},
    {"setssid", new SetSsidCmd()},
    {"getpass", new GetPassCmd()},
    {"setpass", new SetPassCmd()},
    {"restartwifi", new RestartWiFICmd()},
};

bool Cli::read_line()
{
    while (stream.available())
    {
        char datarcv = stream.read();
        if (!isprint(datarcv) && (datarcv != 13))
        {
            continue;
        }

        if (datarcv != 13)
        {
            line_string.concat(datarcv);
            stream.print(datarcv);
        }
        else
        {
            stream.println("");

            if (line_string.length() < LINE_BUF_SIZE)
            {
                line_string.toCharArray(line, LINE_BUF_SIZE);
            }
            else
            {
                // error_flag = true;
            }
            stream.flush();
            line_string = "";
            return true;
        }
    }
    return false;
}

void Cli::init()
{
    stream.print("> ");
}

void Cli::process()
{
    if (read_line())
    {
        split(line, " ", parts);
        if (parts.size())
        {
            auto it = commands.find(parts[0]);
            if (it != commands.end())
            {
                if (parts.size() >= 2)
                {
                    if (parts[1] == "help" || parts[1] == "--help" || parts[1] == "-h")
                    {
                        stream.println(it->second->help());
                    }
                    else
                    {
                        stream.println(it->second->cmd(parts));
                    }
                }
                else
                {
                    stream.println(it->second->cmd(parts));
                }
            }
        }
        parts.clear();
        stream.print("> ");
    }
}
