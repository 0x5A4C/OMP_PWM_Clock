#include <mod/pcMonPresenter.h>
#include <mod/log.h>

void init_pcMonPresenter()
{
  Logger.debug.println("pcMonPresenter: init");
}

void process_pcMonPresenter()
{
  Logger.debug.println("pcMonPresenter: process");
}

void close_pcMonPresenter()
{
  Logger.debug.println("pcMonPresenter: close");
}
