#include <mod/setUpWiFi.h>
#include <mod/log.h>

#include <ESP8266WiFi.h>

#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <EEPROM.h>

const char *ssid = "zxc";		   // insert your own ssid
const char *password = "1234!!!!"; // and password
// WiFiManager wifiManager;

// #define CREDENTIALS_LEN 20
// #define SSID_ADDR 0
// #define PASS_ADDR (SSID_ADDR + CREDENTIALS_LEN)
// String ssidStr = "";
// String passStr = "";

// String getSsid()
// {
// 	EEPROM.begin(512);
// 	EEPROM.get(SSID_ADDR, ssidStr);
// 	return ssidStr;
// }

// void setSsid(String newSsidStr)
// {
// 	EEPROM.begin(512);
// 	EEPROM.put(SSID_ADDR, newSsidStr);
// 	EEPROM.commit();
// 	ssidStr = newSsidStr;
// }

// String getPass()
// {
// 	EEPROM.begin(512);
// 	EEPROM.get(PASS_ADDR, passStr);
// 	return passStr;
// }

// void setPass(String newPassStr)
// {
// 	EEPROM.begin(512);
// 	EEPROM.put(PASS_ADDR, newPassStr);
// 	EEPROM.commit();
// 	passStr = newPassStr;
// }

// void restartWiFI_setUpWiFi()
// {
// 	WiFi.disconnect();
// 	WiFi.begin(ssidStr, passStr);
// }

// String getIpAsString_setUpWiFi()
// {
// 	return WiFi.localIP().toString();
// }

void init_setUpWiFi()
{
	Logger.debug.println("module: init");

	// wifiManager.resetSettings();
	// wifiManager.autoConnect();

	WiFi.begin(ssid, password);
	while (WiFi.status() != WL_CONNECTED)
	{
		delay(500);
		Logger.debug.print(".");
	}
}

void process_setUpWiFi()
{
	// Logger.debug.println("module: process");
}

void close_setUpWiFi()
{
	Logger.debug.println("module: close");
}
