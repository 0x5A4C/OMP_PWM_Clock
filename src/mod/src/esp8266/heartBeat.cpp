#include <mod/log.h>

#include <ESP8266WiFi.h>
#include <Ticker.h>

Ticker timer_update;

const int PIN_LED = 2; // D4 on NodeMCU and WeMos. Controls the onboard LED.

void updateState()
{
  digitalWrite(PIN_LED, !digitalRead(PIN_LED));
}

void init_heartBeat()
{
  Logger.debug.println("heartBeat: init");
  pinMode(PIN_LED, OUTPUT);
  timer_update.attach(1, updateState);
}

void process_heartBeat()
{
  // Logger.debug.println("heartBeat: process");
}

void close_heartBeat()
{
  Logger.debug.println("heartBeat: close");
}
