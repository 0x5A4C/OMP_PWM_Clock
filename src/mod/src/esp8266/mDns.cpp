#include <mod/mDns.h>
#include <mod/mDnsCfg.h>
#include <mod/log.h>

#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

ESP8266WebServer web_server_info(8888);
MDNSResponder mdns;

void start_Dns()
{
    MDNS.end();
    if (MDNS.begin(getName().second, WiFi.localIP()))
    {
        Serial.println("MDNS responder started");
        Serial.println(WiFi.localIP());
    }
    MDNS.addService("http", "tcp", 80);
    MDNS.addService("telnet", "tcp", 23);
}

void handle_info()
{
    String info;

    info += getName().first + String(": ") + getName().second + String("\n");
    info += getIp().first + String(": ") + getIp().second + String("\n");

    web_server_info.send(200, "text/plain", info);
}

void init_mDns()
{
    Logger.debug.println("module: init");

    web_server_info.on("/info", handle_info);
    web_server_info.begin();

    start_Dns();
}

void process_mDns()
{
    // Logger.debug.println("module: process");
    web_server_info.handleClient();
    MDNS.update();
}

void close_mDns()
{
    Logger.debug.println("module: close");
}
