#include <mod/i2c.h>
#include <mod/log.h>

#include <Wire.h>

void init_i2c()
{
	Logger.debug.println("i2c: init");

	Wire.begin(0, 2);         // D3 and D4 on ESP8266
}

void process_i2c()
{
	// Logger.debug.println("i2c: process");
}

void close_i2c()
{
	Logger.debug.println("i2c: close");
}
