#include <mod/displaySer.h>
#include <mod/log.h>

#include <Arduino.h>
#include <Ticker.h>
#include <Time.h>

#define LOG_TO_SERIAL 1

Ticker timer_displaySer;

bool flag = true;

void printLeading0(int n)
{
#if LOG_TO_SERIAL
	if(n < 10)
		Logger.debug.print("0");

	Logger.debug.print(n);
#endif
}

void printTime(const char s[],time_t t)
{
#if LOG_TO_SERIAL
	flag =false;
	Logger.debug.print(s);
	printLeading0(hour(t));
	Logger.debug.print(":");
	printLeading0(minute(t));
	Logger.debug.print(":");
	printLeading0(second(t));
	// Logger.debug.print(" ");
	// Logger.debug.print(dayStr(weekday(t)));
	// Logger.debug.print(" ");
	// Logger.debug.print(day(t));
	// Logger.debug.print(" ");
	// Logger.debug.print(monthStr(month(t)));
	// Logger.debug.print(" ");
	// Logger.debug.print(year(t));
	Logger.debug.println();
#endif
}

void printFormatedTime()
{
	printTime(" ",now());
}

void init_dispSer()
{
	Logger.debug.println("sispSer: init");

	timer_displaySer.attach(1, printFormatedTime);
}

void process_dispSer()
{
}

void close_dispSer()
{
	Logger.debug.println("dispSer: close");
}
