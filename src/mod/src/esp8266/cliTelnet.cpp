#include <mod/cliTelnet.h>
#include <mod/cli.h>

#include <ESP8266WiFi.h>
#include <Arduino.h>

WiFiServer ser(23);
// WiFiClient client;
// Cli cliTelnet = Cli(client);

void init_cliTelnet()
{
    ser.begin();
    // client = ser.available(); // initialise client object with status of the server.
}

void process_cliTelnet()
{

    // cliTelnet.init();
    WiFiClient client = ser.available(); // initialise client object with status of the server.
    Cli cliTelnet = Cli(client);
    // cliTelnet.init();

    while (client)
    {
        if (client.connected())
        {
            Serial.println("client connected"); // if telnet terminal is accessed, notify the user that client is connected.
        }

        while (client.connected())
        { // while client is connected stay in the loop.
            cliTelnet.process();

        }
        client.stop();                  // if client is disconnected, stop the transfer.
        Serial.println("disconnected"); // notify the user that client.
    }
}

void close_cliTelnet()
{
}