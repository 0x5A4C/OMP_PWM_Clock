#include <mod/module.h>
#include <mod/log.h>

void init_Module()
{
  Logger.debug.println("module: init");
}

void process_Module()
{
  Logger.debug.println("module: process");
}

void close_Module()
{
  Logger.debug.println("module: close");
}
