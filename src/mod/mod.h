#ifndef MOD
#define MOD

namespace Mod
{
extern void modInit();
extern void modLoop();
} // namespace Mod

#endif
