#include <mod/mDnsCfg.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>

std::pair<String, String> getName()
{
    auto name = String("Name");
    auto value = String("cl_") + String(WiFi.localIP()[3]);
    return {name, value};
}
std::pair<String, String> getIp()
{
    auto name = String("Ip");
    auto value = String(WiFi.localIP().toString());
    return {name, value};
}
