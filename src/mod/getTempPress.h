
#ifndef GET_TEMP_PRESS
#define GET_TEMP_PRESS

float temp_getTempPress();
float press_getTempPress();
int32_t alt_getTempPress();

void init_getTempPress();
void process_getTempPress();
void close_getTempPress();

#endif
