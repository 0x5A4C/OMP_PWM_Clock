#ifndef CLI
#define CLI

#include <Arduino.h>

#include <vector>
#include <string>
#include <map>

#define LINE_BUF_SIZE 128 //Maximum input string length

class Splitter
{
protected:
    static void split(const std::string &str, const std::string &delim, std::vector<std::string> &parts);
};

class Cli : public Splitter
{
private:
    Stream &stream;

    char line[LINE_BUF_SIZE];
    std::vector<std::string> parts;
    String line_string = "";
    bool read_line();

public:
    Cli(Stream &str) : stream(str){};

    void init();
    void process();
};

#endif
